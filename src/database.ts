import { Connection } from "mongoose";
import * as mongoose from "mongoose";
import { dbLogger } from "./logger";
// import { Mockgoose } from "mockgoose";
mongoose.set("debug", (coll, method, query, doc, options) => {
  dbLogger.log({
    level: "info",
    source: "db",
    message: JSON.stringify({
      coll,
      method,
      query,
      doc,
      options
    })
  });
});
(<any>mongoose).Promise = global.Promise;
/**
 * Connect to MongoDB.
 */
// export class DatabaseConnection {
//     constructor(private role: string) {

//     }
//     async connect() {
//             switch (this.role) {
//                 case "admin": {
//                     const admin_uri = process.env.MONGODB_URI_ADMIN;
//                     // console.log("reading", process.env, admin_uri);
//                     console.log("Admin connected");
//                     return await mongoose.createConnection(admin_uri, { useMongoClient: true, });
//                 }
//                 case "merchant": {
//                     const merchant_uri = process.env.MONGODB_URI_MERCHANT;
//                     console.log("reading", merchant_uri);
//                     console.log("Merchant connected");
//                     return await mongoose.createConnection(merchant_uri, { useMongoClient: true, });
//                 }
//                 case "buyer": {
//                     const buyer_uri = process.env.MONGODB_URI_BUYER;
//                     console.log("reading", buyer_uri);
//                     return await mongoose.createConnection(buyer_uri, { useMongoClient: true, })
//                         .on("connected", () => {
//                             console.log("Buyer connected");
//                             console.log("I am opened only once!");
//                         })
//                         .on("error", () => {
//                             console.log("Whoops");
//                             console.log("Something wrong!");
//                         })
//                         .on("closed", () => {
//                             console.log("I am closed!");
//                         });
//                 }
//             }
//         }
//     }

export default async (role: string) => {
  let connection: Connection;
  if (process.env.NODE_ENV === "test") {
    // const mockgoose = new Mockgoose(mongoose);
    // await mockgoose.prepareStorage();
    connection = await mongoose.connect("mongodb://localhost/", {
      useMongoClient: true
    });
    console.log("Mock DB connected");
  } else {
    switch (role) {
      case "admin": {
        const admin_uri = "mongodb://localhost:27017";
        // process.env.MONGODB_URI_ADMIN;
        // console.log("reading", process.env, admin_uri);
        connection = await mongoose.createConnection(admin_uri, {
          useMongoClient: true
        });
        console.log("Admin connected");
        break;
      }
      case "merchant": {
        const merchant_uri = "mongodb://localhost:27017";
        // process.env.MONGODB_URI_MERCHANT;
        console.log("reading", merchant_uri);
        connection = await mongoose.createConnection(merchant_uri, {
          useMongoClient: true
        });
        console.log("Merchant connected");
        break;
      }
      case "buyer": {
        const buyer_uri = "mongodb://localhost:27017";
        // process.env.MONGODB_URI_BUYER;
        console.log("reading", buyer_uri);
        connection = await mongoose
          .createConnection(buyer_uri, { useMongoClient: true })
          .on("connected", () => {
            console.log("Buyer connected");
            console.log("I am opened only once!");
          })
          .on("error", () => {
            console.log("Whoops");
            console.log("Something wrong!");
          })
          .on("closed", () => {
            console.log("I am closed!");
          });
        break;
      }
    }
  }
  return {
    userDB: connection.useDb("production"),
    productDB: connection.useDb("production"),
    orderDB: connection.useDb("production"),
    role
  };
};

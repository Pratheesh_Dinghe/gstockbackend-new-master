import routes from "./routes";
import { Express } from "express";
import { getUserPassport, UserPassport } from "./config/user.passport";
import { getAdminPassport, AdminPassport } from "./config/admin.passport";
import getModels, { Models } from "./models";
import { Config } from "./types/app";
const {
  orderRoutes,
  ProductRoutes,
  attributeMgmtRoute,
  categoryMgmtRoute,
  productMgmtRoute,
  promotionMgmtRoute,
  rewardProductMgmtRoute,
  EmailMgmtRoutes,
  userMgmtRoute,
  BuyerRoutes,
  UserRoutes,
  merchantRoutes,
  adminRoute,
  logisticRoute,
  orderMgmtRoute
} = routes;
export class API {
  app: Express;
  config: Config;
  constructor(app: Express, config: Config) {
    this.app = app;
    this.config = config;
    switch (config.role) {
      case "admin": {
        this.regsiterAdmin();
        return;
      }
      case "buyer": {
        this.regsiterBuyer();
        return;
      }
      case "merchant": {
        this.regsiterMerchant();
        return;
      }
      default:
        throw "Invalid user";
    }
  }
  private regsiterAdmin() {
    const config = this.config;
    const app = this.app;
    const { userPassport } = config.passport;
    console.log("Connecting as admin");
    app.use("/admin", adminRoute(config));
    app.use("/admin/attributes", attributeMgmtRoute(config));
    app.use("/admin/category", categoryMgmtRoute(config));
    app.use("/admin/product", productMgmtRoute(config));
    app.use("/admin/promotion", promotionMgmtRoute(config));
    app.use("/admin/reward-product", rewardProductMgmtRoute(config));
    app.use("/admin/user", userMgmtRoute(config));
    app.use("/admin/order", orderMgmtRoute(config));
    app.use("/admin/logistic", logisticRoute(config));
    app.use("/admin/email", new EmailMgmtRoutes(config).register());
    app.use("/user", new UserRoutes(config).register());
    app.use("/buyer", new BuyerRoutes(config).register());
    app.use("/merchant", merchantRoutes(config));
    app.use("/product", new ProductRoutes(config).register());
    app.use("/order", orderRoutes(config));
    const swaggerUi = require("swagger-ui-express");
    const swaggerJson = require("./swagger.json");
    app.use(
      "/api-docs",
      swaggerUi.serve,
      swaggerUi.setup(swaggerJson, { showExplorer: true })
    );
  }
  private regsiterBuyer() {
    const config = this.config;
    const app = this.app;
    const { userPassport } = config.passport;
    console.log("Connecting as buyer");
    /**
     * POST /user/login
     * POST /user/register
     * POST /user/register/fb
     * POST /user/forgot/reset
     * POST /user/forgot/email
     * POST /user/forgot/token
     * GET /user/account/detail
     * PUT /user/account/detail
     * POST /user/account/reset
     */
    app.use("/user", new UserRoutes(config).register());
    /**
     * GET /buyer/cart
     * POST /buyer/cart
     * PUT /buyer/cart
     * DELETE /buyer/cart/:cart_id
     */
    app.use(
      "/buyer",
      userPassport.isJWTValid.bind(userPassport),
      new BuyerRoutes(config).register()
    );
    /**
     * GET /product/list
     * GET /product/product
     * GET /product/category
     * GET /product/variants/:id
     */
    app.use("/product", new ProductRoutes(config).register());
    /**
     * POST /order/place
     * GET /product/product
     * GET /product/category
     * GET /product/variants/:id
     */
    // app.use("/order", orderRoutes(config));
    app.use("/order", userPassport.isJWTValid.bind(userPassport), orderRoutes(config));
    return;
  }
  private regsiterMerchant() {
    const config = this.config;
    const app = this.app;
    const { userPassport } = config.passport;
    console.log("Connecting as merchant");
    /**
     * POST /user/login
     * POST /user/register
     * POST /user/forgot/reset
     * POST /user/forgot/email
     * POST /user/forgot/token
     * GET /user/account/detail
     * PUT /user/account/detail
     * POST /user/account/reset
     */
    app.use("/user", new UserRoutes(config).register());
    /*
     * POST /merchant/product
     * POST /user/register
     * POST /user/forgot/reset
     * POST /user/forgot/email
     * POST /user/forgot/token
     * GET /user/account/detail
     * PUT /user/account/detail
     * POST /user/account/reset
     */
    app.use(
      "/merchant",
      userPassport.isJWTValid.bind(userPassport),
      merchantRoutes(config)
    );
    app.use("/product", new ProductRoutes(config).register());
    app.use("/order", orderRoutes(config));
    return;
  }
}

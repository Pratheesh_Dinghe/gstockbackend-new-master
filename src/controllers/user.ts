import { NextFunction, Request, Response } from "express";
import { IVerifyOptions } from "passport-local";
import { sendEmail } from "./../emailTemplate/email_manager";
import * as jwt from "jsonwebtoken";
import { ObjectID } from "bson";
import { Config } from "../types/app";
import * as flat from "flat";
import { Passport } from "passport";
import { More } from "../_lib/decorators";
import { BaseController } from "../_lib/crud";
import { PaginateModel } from "mongoose";

export class UserController extends BaseController {
  public models;
  public userPassport: Passport;
  constructor(private config: Config) {
    super(config.models.User);
    this.models = config.models;
    this.userPassport = config.passport.userPassport.getUserPassport();
  }
  @More({
    "100000": {
      message: "Login successful.",
      detail: "Feel like being talktive?"
    },
    "100001": {
      message: "Account does not exist.",
      detail: "Does not look good mate."
    },
    "100002": {
      message: "Invalid password.",
      detail: "Does not look good mate."
    }
  })
  public async login(req: Request, res: Response, next: NextFunction) {
    try {
      // const user: UserModel = await this.create({});
      req.sanitize("email").normalizeEmail({ gmail_remove_dots: false });
      const { email, user_group } = req.body;
      return await new Promise((resolve, reject) =>
        this.userPassport.authenticate(
          "local",
          { session: false },
          (err: Error, user, info: IVerifyOptions) => {
            if (err) return reject(err);
            if (!user) return reject({ ...info });
            const payload = {
              id: user.id,
              user_group,
              timestamp: new Date(),
              expired: 900
            };
            const token = jwt.sign(payload, process.env.JWTKEY);
            resolve({
              status_code: 200,
              detail_code: 100000,
              token,
              user_id: user._id,
              email: user.credential.email
            });
          }
        )(req, res, next)
      );
    } catch (error) {
      return error.status_code ? error : next(error);
    }
  }
  public async fbRegister(req: Request, res: Response, next: NextFunction) {
    const { email, contact_no, first_name, last_name, id: fb_id } = req.body;
    const errors = await req.getValidationResult();
    if (!errors.isEmpty()) {
      return res.status(400).json(errors.array({ onlyFirstError: true }));
    }
    const oldUser = await this.models.User.findOne({
      "credential.fb_id": fb_id
    });
    if (oldUser) {
      // pass old user info to next handler
      res.locals.user = oldUser;
      // if old user, set welcome email flag to false
      res.locals.welcome = false;
      return next();
    }
    // check if a new user has fb email in conflict
    if (!email) return res.status(404).json("Email is needed!");
    const emailConflict = await this.models.User.findOne({
      "credential.email": email
    }).lean();
    if (emailConflict)
      return res.status(409).json({ message: "Account existed" });
    const newUser = await new this.models.User({
      credential: {
        email,
        // by default fb regsiter is only open to buyer
        user_group: "buyer",
        fb_id
      },
      profile: {
        first_name,
        last_name
      }
    }).save();
    res.locals.user = newUser;
    // if new user, set welcome email flag to true
    res.locals.welcome = true;
    return next();
  }
  public async register(req: Request, res: Response, next: NextFunction) {
    req.sanitize("email").normalizeEmail({ gmail_remove_dots: false });
    const {
      email,
      contact_no,
      password,
      store_name,
      user_group,
      first_name,
      last_name
    } = req.body;
    if ("buyer" !== user_group && "merchant" !== user_group) {
      return res.status(400).send({ message: "Unknown user group!" });
    }
    if (await this.models.User.findOne({ "credential.email": email }).lean()) {
      return res.status(409).json({ message: "Account existed" });
    }
    const userBase = {
      credential: {
        email,
        user_group,
        password
      },
      profile: {
        first_name,
        last_name
      },
      contact: {
        mobile_no: contact_no
      }
    };
    let newUser;
    if ("merchant" === user_group) {
      if (!store_name)
        return res.status(400).json({ message: "Store name cannot be empty." });
      if (await this.models.User.findOne({ "store.name": store_name }).lean())
        return res.status(409).json({ message: "Store name existed" });
      newUser = {
        ...userBase,
        store: {
          name: store_name
        }
      };
    } else {
      newUser = userBase;
    }
    const user = await new this.models.User(newUser).save();
    res.locals.user = user;
    return next();
  }
  public async reset(req: Request, res: Response, next: NextFunction) {
    try {
      const { password, old_password } = req.body;
      const user = await this.models.User.findById(res.locals.user.id).select(
        "credential"
      );
      if (!user) return res.status(400).send({ message: "No such user." });
      user.comparePassword(
        old_password,
        user.credential.password,
        async (err, isMatched) => {
          if (!isMatched)
            return res.status(400).send({ message: "Wrong password!" });
          user.credential.password = password;
          await user.save();
          return res.status(200).send({ message: "Password changed" });
        }
      );
    } catch (error) {
      return next(error);
    }
  }
  public async forgot(req: Request, res: Response, next: NextFunction) {
    const { email, token, new_pw } = req.body;
    try {
      const user = await this.models.User.findOne({
        "credential.email": email
      });
      if (user.credential.email_verif_token !== token)
        return res.status(400).send({ message: "Invalid token!" });
      user.credential.password = new_pw;
      await user.save();
      return res.status(200).send({ message: "Password updated!" });
    } catch (error) {
      return next(error);
    }
  }
  public async generateToken(req: Request, res: Response, next: NextFunction) {
    const {
      user: {
        id,
        credential: { email, user_group }
      }
    } = res.locals;
    // send token only on registration
    const payload = {
      id,
      user_group,
      timestamp: new Date(),
      expired: 900
    };
    const token = jwt.sign(payload, process.env.JWTKEY);
    return res.status(200).send({
      token,
      email
    });
  }
  public async updateEmailVerifToken(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    const { email, token } = req.query;
    const user = await this.models.User.findOneAndUpdate(
      {
        "credential.email": email,
        "credential.email_verif_expires": { $gt: Date.now() },
        "credential.email_verif_token": token
      },
      {
        $set: { "credential.email_verified": true }
      }
    ).lean();
    return user
      ? res.status(200).json({ message: "Email verfied" })
      : res.status(404).json({ message: "No valid user found" });
  }
  @More()
  public async getUserAccountDetail(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    return { status_code: 200, detail_code: 0, ...res.locals.user.toJSON() };
  }
  @More()
  public async updateUserAccountDetail(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      console.log(req.locals.user.id,req.body)
      const result = await this.models.User.findByIdAndUpdate(
        req.locals.user.id,
        { $set: flat.flatten(req.body) },
        { new: true }
      );
      return {
        status_code: 200,
        detail_code: 0,
        ...result.toJSON()
      };
    } catch (error) {
      return error.status_code ? error : next(error);
    }
  }
  @More()
  public async getRewardPts(req: Request, res: Response, next: NextFunction) {
    try {
      return {
        status_code: 200,
        detail_code: 0,
        reward_points: await this.models.RewardPts.find({
          buyer_id: req.locals.user.id
        })
      };
    } catch (error) {
      return error.status_code ? error : next(error);
    }
  }
  // public async getStore(req: Request, res: Response, next: NextFunction) {
  //   console.log("getStore");

  //   console.log("MERCHANT ", req.query);

  //   try {
  //     const { merchant_id } = req.query;

  //     const detail = await this.models.User.findById({
  //       _id: new ObjectID(merchant_id),
  //       "credential.user_group": "merchant"
  //     }).select("store createdAt");

  //     console.log("MERCHANT DETAILSSSS", detail);
  //     if (!detail)
  //       return res.status(200).json({ message: "No such merchant!" });
  //     const products = await this.models.Product.find({
  //       merchant_id: new ObjectID(merchant_id),
  //       status: "Approved"
  //     }).select("brief pricing");
  //     return res.status(200).send({
  //       detail,
  //       products
  //     });
  //   } catch (e) {
  //     return next(e);
  //   }
  // }

  // public async getStore(req: Request, res: Response, next: NextFunction) {
  //   console.log("getStore");
  //   console.log("MERCHANT ", req.query);

  //   try {
  //     const { merchant_id } = req.query;

  //     console.log("MERCHANT IDDDD", merchant_id);
  //     const detail = await this.models.User.findOne({
  //       _id: new ObjectID(merchant_id),
  //       "credential.user_group": "merchant"
  //     }).select("store createdAt");

  //     console.log("MERCHANT DETAILSSSS", detail);
  //     if (!detail)
  //       return res.status(200).json({ message: "No such merchant!" });
  //     const products = await this.models.Product.find({
  //       merchant_id: new ObjectID(merchant_id),
  //       status: "Approved"
  //     }).select("brief pricing");
  //     return res.status(200).send({
  //       detail,
  //       products
  //     });
  //   } catch (e) {
  //     return next(e);
  //   }
  // }





  public async getStore(req: Request, res: Response, next: NextFunction) {
    console.log("getStore");
    console.log("MERCHANT ", req.query);

    try {
      const { merchant_id } = req.query;

      console.log("MERCHANT IDDDD", merchant_id);
      const detail = ()=>  this.models.User.findOne({
        _id: new ObjectID(merchant_id),
        "credential.user_group": "merchant"
      }).select("store createdAt");

      console.log("MERCHANT DETAILSSSS", detail);
      if (!detail)
        return res.status(200).json({ message: "No such merchant!" });
      const products = ()=> this.models.Product.find({
        merchant_id: new ObjectID(merchant_id),
        status: "Approved"
      }).select("brief pricing");
      return res.status(200).send({
        detail,
        products
      });
    } catch (e) {
      console.log(e)
      return next(e);
    }
  }

  public async getCreditWalletHistory(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const findTotal = await this.models.CreditWallet.aggregate([
        {
          $match: {
            buyer_id: new ObjectID(req.locals.user.id)
          }
        },
        {
          $group: {
            _id: "",
            total: { $sum: "$amount" }
          }
        },
        {
          $project: {
            _id: 0,
            total: "$total"
          }
        }
      ]);

      return res.status(200).send({
        data: {
          history: await this.models.CreditWallet.find({
            buyer_id: new ObjectID(req.locals.user.id)
          }),
          ...findTotal[0]
        }
      });
    } catch (error) {
      return next(error);
    }
  }
}

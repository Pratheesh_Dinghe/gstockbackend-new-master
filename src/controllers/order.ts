import { NextFunction, Request, Response } from "express";
import { Types } from "mongoose";

// import { Config } from "./../app";
import { sendEmail } from "./../emailTemplate/email_manager";
import { db } from "./../firebase";
import commentCtrl from "./comment";
import { ORDER_STATUS, PRODUCT_STATUS, COMMISSION_STATUS } from "./../../src/_status/status";
import { calculateDiscountPrice } from "../_global/business";
import { MerchantCommission } from "../models/User";
const debug = require("debug")("express:router");

interface OrderAccepted {
    carts: Array<string>;
    orders:
    [{
        merchant_id: string,
        products: [{
            product_id: string;
            "variants": [
                {
                    cart_id: string,
                    variant_id: string;
                    "qty": number
                }
            ]
        }],
        delivery: [{
            "address": string;
            "unit_no": string;
            "postal_code": string;
            "bill_to": string;
            "recepient": string;
            "contact_no": string;
            "shipping_fee": number;
            "shipping_type": string;
        }]
    }];
    promo_codes: Array<string>;
}
/*}
 * Order controller.
 */
function decimal(num, sf = 2) {
    return Math.round(num * Math.pow(10, sf)) / Math.pow(10, sf);
}

export default (config) => {
    const {
        User,
        Product,
        Category,
        Variant,
        Order,
        Promotion,
        RewardPts,
        Cart,
        Complaint
    } = config.models;
    async function getDeliveryDetail(req: Request, res: Response, next: NextFunction) {
        // if (!req.body.delivery) return res.status(400).json({ "message": "Invalid delivery form." });
        console.log("getDeliveryDetail")
        console.log("2222222222222222");
        return next();
    }
    async function getOrderDetail(req: Request, res: Response, next: NextFunction) {
        try {
            console.log("getOrderDetail")
            console.log("333333333333")
            const {
                // promo_codes: promoCodes,
                orders
            } = req.body as OrderAccepted;
            await User.populate(orders, { path: "merchant_id", select: "commission" });
            await Product.populate(orders, { path: "products.product_id", select: "brief pricing category_id" });
            await Category.populate(orders, { path: "products.product_id.category_id", select: "commission" });
            await Variant.populate(orders, { path: "products.variants.variant_id" });
            res.locals.orderDetails = orders;
            return next();
        } catch (error) {
            console.log(error)
            throw ("Failed to get order details");
        }
    }
    async function getPromotionDetailFromPromoCode(req: Request, res: Response, next: NextFunction) {
        try {
            console.log("getPromotionDetailFromPromoCode")

            console.log("44444444444444444");
            const { promo_codes: promoCodes } = req.body as OrderAccepted;
            const promotions = await Promotion.find({ promo_code: { $in: promoCodes }, status: "Active" });

            const firstOrderOnlyPromo = promotions.filter(p => p.first_order);
            if (firstOrderOnlyPromo.length) {
                const order = await Order.findOne({
                    buyer_id: res.locals.user.id,
                    "promotion.promo_code": { $in: firstOrderOnlyPromo.map(f => f.promo_code) }
                });
                if (order) { return res.status(400).send({ "message": "You have used promotion code." }); }
            }
            res.locals.promotions = promotions;
            next();
        } catch (error) {
            console.log(error)
            throw ("Failed to verify promotion codes");
        }
    }
    function processStore(req: Request, res: Response, next: NextFunction) {
        console.log("processStore")
        console.log("555555555555");
        const userId = req["user_id"];
        const {
            orderDetails,
            promotions
        } = res.locals;
        const carts = [];
        const ordersForReport = orderDetails.map(
            store => {
                const {
                    memo,
                    merchant_id: {
                        _id: merchantId
                    },
                    delivery
                } = store;
                const {
                    cart,
                    products,
                    freeShipping
                } = processProducts(store, promotions);
                carts.push(...cart);
                const total = calculateTotal(products);
                const {
                    product_total_bp: productTotalBefPromo,
                    product_total_ap: productTotalAftPromo,
                    commission_amount: commissionAmount
                } = total;
                total["store_bp"] = productTotalBefPromo;
                total["store_ap"] = productTotalAftPromo;
                total["commission"] = commissionAmount;
                Object.keys(total).forEach(k => total[k] = decimal(total[k]));
                return {
                    memo,
                    buyer_id: userId,
                    merchant_id: merchantId,
                    free_shipping: freeShipping,
                    delivery,
                    products,
                    total,
                    promotions
                };
            });
        res.locals.cart = carts;
        res.locals.orderForReport = ordersForReport;
        next();
    }
    function processProducts(store, promotions) {
        const cart = [];
        let freeShipping = false;
        const products = store.products
            .map(p => {
                // product["_id"] = productId;
                // const commission = calculateCommission(category, specialCommissionAssignedToMerchant);
                const {
                    freeShipping: free_shipping,
                    product_total_bp,
                    product_total_ap,
                    variantsAftProc,
                    cart: cartsFromVariant,
                    total_commission: commission,
                    total_reward_pts: reward_pts
                } = processVariants(store, p, promotions);
                if (freeShipping || free_shipping) { freeShipping = true; }
                cart.push(...cartsFromVariant);
                return {
                    purchase: {
                        product_total_bp,
                        product_total_ap,
                        commission,
                        commission_amount: product_total_bp * commission,
                        /* total * commission_rate * reward_pts_rate * 100 */
                        reward_pts
                        // : calculateRewardPts(product_total_bp, commission, 0.05)
                    },
                    product: p.product_id,
                    variants: variantsAftProc,
                };
            });
        return {
            products,
            cart,
            freeShipping
        };
    }
    function processVariants(store, baseProduct, promotions) {
        const {
            merchant_id: {
                _id: merchantId,
                commission: specialCommissionAssignedToMerchant
            }
        } = store;
        const {
            product_id: product,
            variants: variantsBefProc
        } = baseProduct;
        const {
            category_id: category,
            category_id: {
                _id: categoryId
            },
            brief: {
                discount,
                price: priceBefDiscount,
            },
            pricing: {
                discount_rate,
            }
        } = product;
        const variantsHolder = [];
        const cart = [];
        let product_total_bp = 0;
        let product_total_ap = 0;
        let total_commission = 0;
        let total_reward_pts = 0;
        let freeShipping = false;
        variantsBefProc.forEach(
            (v) => {
                const {
                    variant_id: variant,
                    qty,
                    cart_id
                } = v;
                const price = variant ? variant["price"] : priceBefDiscount;
                const priceAftDiscount = decimal(discount ? calculateDiscountPrice(price, discount_rate) : price);
                let toPush = { "order_qty": qty };
                const totalPriceAfterDiscount = priceAftDiscount * qty;
                product_total_bp += totalPriceAfterDiscount;
                const commission = decimal(calculateCommission(category, specialCommissionAssignedToMerchant) * totalPriceAfterDiscount);
                total_commission += commission;
                const priceAftDiscountAndCommission = totalPriceAfterDiscount - commission;
                const reward_pts = decimal(calculateRewardPts(priceAftDiscountAndCommission, 0.05));
                total_reward_pts += reward_pts;
                const {
                    product_ap,
                    free_shipping
                } = calPriceAftPromo(promotions, categoryId, merchantId, priceAftDiscount);
                if (free_shipping || freeShipping) { freeShipping = free_shipping; }
                product_total_ap += product_ap * qty;
                if (undefined !== variant) toPush = {
                    ...toPush,
                    ...variant.toObject(),
                    commission,
                    reward_pts,
                };
                variantsHolder.push(toPush);
                cart.push(cart_id);
            }
        );
        return {
            cart,
            total_commission,
            total_reward_pts,
            product_total_ap,
            product_total_bp,
            variantsAftProc: variantsHolder,
            freeShipping
        };
    }
    async function removeItemFromCart(cart, user_id) {
        const carts = cart.map(c => Types.ObjectId(c));
        try {
            return await Cart.remove({
                _id: { $in: carts },
                buyer_id: Types.ObjectId(user_id)
            });
        } catch (error) {
            throw (error);
        }
    }
    async function updatePendingStock(ordersInserted, pending_stock) {
        const r = pending_stock.map(e =>
            ({
                order_id: ordersInserted.find(o => o.merchant_id.equals(e.merchant_id))._id,
                ...e
            })
        );
        await Promise.all(
            r.map((l) => Product.findByIdAndUpdate(l.product_id, {
                $push: {
                    "stock.pending": l
                }
            })));
        return r;
    }
    async function updateRewardPts(ordersInserted, user_id) {
        let totalPts = 0;
        const rewardPts = ordersInserted.map(
            o => {
                totalPts += o.total.reward_pts;
                return {
                    buyer_id: user_id,
                    order_id: o._id,
                    pts: o.total.reward_pts,
                    status: "Pending"
                };
            }
        );
        await RewardPts.insertMany(rewardPts);
        return totalPts;
    }
    async function sendEmailAndNotification(user, payload) {
        const {
            orderStatus,
            ordersInserted: orders,
            totalRewardPts
        } = payload;
        sendEmail("NEW_ORDER", user, { totalRewardPts });
        await sendEmailAndNotificationToMerchant("MER_ORDER", user, { orders, orderStatus });
    }
    async function sendEmailAndNotificationToMerchant(emailType, merchant, payload) {
        const {
            orderStatus,
            orders,
            totalRewardPts
        } = payload;
        sendEmail("MER_ORDER", merchant, { orderStatus });
        const newNewsKey = db.ref("/news").push().key;
        const updates = {};
        return Promise.all(orders.map((m) => {
            updates[`/news/${m.merchant_id}/${newNewsKey}`] = {
                "order_id": m._id,
                "type": "order",
                "create_date": new Date(),
                "read": false,
                "status": orderStatus
            };
            return db.ref("/").update(updates);
        }));
    }
    // async function sendEmailAndNotification(ordersInserted, user_id, totalRewardPts) {
    //     const user = await User.findById(user_id);
    //     console.log(totalRewardPts);
    //     sendEmail("NEW_ORDER", user, { totalRewardPts: totalRewardPts });
    //     sendEmail("MER_NEW_ORDER", user, { totalRewardPts: totalRewardPts });
    //     const newNewsKey = db.ref("/news").push().key;
    //     const updates = {};
    //     await Promise.all(ordersInserted.map((m) => {
    //         updates[`/news/${m.merchant_id}/${newNewsKey}`] = {
    //             "order_id": m._id,
    //             "type": "order",
    //             "create_date": new Date(),
    //             "read": false,
    //             "status": "Pending"
    //         };
    //         return db.ref("/").update(updates);
    //     }));
    // }
    function calPriceAftPromo(promotions, categoryId, merchantId, up) {
        let totalPromo = 0;
        let fs = false;
        if (promotions && !promotions.length) {
            return {
                free_shipping: false,
                product_ap: up
            };
        }
        promotions.forEach(promotion => {
            const { freeShipping, accPromoAmt } = productPromotionAmt(up, promotion, categoryId, merchantId);
            totalPromo += accPromoAmt;
            fs = freeShipping;
        });
        // const { product_ap, freeShipping } = ;
        return {
            free_shipping: fs,
            product_ap: up - totalPromo
        };
    }
    function productPromotionAmt(product_bp, promotion, categoryId, merchantId) {
        const { kind, promo_type, value } = promotion;
        switch (kind) {
            case "storewide": {
                debug("This is a store wide");
                const { freeShipping, accPromoAmt } = computePromoAmt(product_bp, promo_type, value);
                debug("This is a store wide", freeShipping);
                return {
                    accPromoAmt,
                    freeShipping
                };
            }
            case "category": {
                console.log("This is a promotion by category");
                const found = promotion.target.findIndex(e => e === categoryId);
                if (found > 0) {
                    const { freeShipping, accPromoAmt } = computePromoAmt(product_bp, promo_type, value);
                    return {
                        accPromoAmt,
                        freeShipping
                    };
                }
                break;
            }
            case "merchant": {
                console.log("This is a promotion by merchant");
                console.log(`Checking if ${merchantId} is participating in promotion`);
                const found = promotion.target.findIndex((e: Types.ObjectId) => e.equals(merchantId));
                if (found > 0) {
                    console.log("Yes he is", found);
                    const { freeShipping, accPromoAmt } = computePromoAmt(product_bp, promo_type, value); accPromoAmt;
                } else {
                    console.log("This merchant is not participating in promotion.", promotion);
                }
                return {
                    accPromoAmt: 0,
                    freeShipping: false
                };
            }
            default: throw ("unkown promotion kind");
        }
        // console.log(`Product price before promotion is ${product_bp}. After checking all promotion, the total deduction amount is ${totalPromo}. Product price after promotion is ${product_bp - totalPromo}`);
        // return {
        //     product_ap: product_bp - totalPromo,
        //     freeShipping: fs
        // };
    }
    function computePromoAmt(price, promoType, value) {
        console.log(`Received product price is ${price}, promotion type is ${promoType} the value is ${value}`);
        let freeShipping = false;
        let accPromoAmt = 0;
        switch (promoType) {
            case "fs": {
                freeShipping = true;
                break;
            }
            case "pd": {
                accPromoAmt = decimal(price * Number.parseFloat(value) / 100);
                console.log(`A product with price ${price} after ${value}% of discount is ${accPromoAmt}`);
                break;
            }
            case "fa": {
                accPromoAmt = Number.parseFloat(value);
                break;
            }
        }
        return {
            freeShipping,
            accPromoAmt,
        };
    }
    function calculateTotal(products) {
        return products.map(e => e.purchase).reduce(
            (a, b) => ({
                reward_pts: a.reward_pts + b.reward_pts || 0,
                commission_amount: a.commission_amount + b.commission_amount || 0,
                product_total_bp: a.product_total_bp + b.product_total_bp || 0,
                product_total_ap: a.product_total_ap + b.product_total_ap || 0,
            }));
    }
    function calculateRewardPts(priceAftDiscountAndCommission, rewardRate) {
        return priceAftDiscountAndCommission * rewardRate * 100;
    }
    function calculateCommission(category, specialCommissionAssignedToMerchant: [MerchantCommission]) {
        if (!category) { throw ("Invalid category"); }
        const defaultRate = category["commission"];
        const specialRate = specialCommissionAssignedToMerchant.find(s => s.category_id.equals(category._id));
        return specialRate ? specialRate.rate / 100 : defaultRate / 100;
    }
    async function placeOrder(req: Request, res: Response, next: NextFunction) {
        // a lot complicated lookup and requries no optimization
        try {
            console.log("placeOrder", req.body)

            console.log("66666666666666");
            const {
                orderDetails,
                promotions,
                cart,
                orderForReport,
                user
            } = res.locals;
            const ordersInserted = await Order.insertMany(orderForReport);
            const totalRewardPts = await updateRewardPts(ordersInserted, user.id);
            await removeItemFromCart(cart, user.id);
            const order_id = [].concat(ordersInserted).map(o => o._id);
            // await sendEmailAndNotification(user, { orderStatus: "Pending", ordersInserted, totalRewardPts });
            console.log("heeee")
            return res.status(200).json(order_id);
        } catch (error) {
            return next(error);
        }
    }
    function getOrder(role) {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const {
                    select,
                    order_id,
                    start,
                    end
                } = req.query;
                const query = {};
                let populate = {};
                switch (role) {
                    case "buyer": {
                        query["buyer_id"] = req["user_id"];
                        populate = [{
                            path: "merchant_id",
                            select: "store",
                            model: User
                        },
                        {
                            path: "promotions._id",
                            select: "",
                            model: Promotion
                        }];
                        break;
                    }
                    case "merchant": {
                        query["merchant_id"] = req["user_id"];
                        populate = {
                            path: "buyer_id",
                            select: "profile credential.email",
                            model: User
                        };
                        break;
                    }
                    case "admin": {
                        populate = {
                            path: "buyer_id merchant_id",
                            select: "contact profile credential.email",
                            model: User
                        };
                        break;
                    }
                    default: throw ("No such role");
                }
                if (start & end) { query["createdAt"] = { $gte: start, $lte: end }; }
                if (order_id) {
                    const orders = await Order.findById(order_id).populate(populate);
                    return res.status(200).send(orders);
                }
                const orders = await Order.find(query).populate(populate).sort("-createdAt");
                return res.status(200).send(orders);
            } catch (error) {
                return next(error);
            }
        };
    }
    async function merchantCancelOrder(req: Request, res: Response, next: NextFunction) {
    }
    async function buyerMadePayment(req: Request, res: Response, next: NextFunction) {
        try {
            console.log("buyerMadePayment", req.body)
            const {
                order_id,
                paypal
            } = req.body;
            const orderIds = [].concat(order_id).map(o => Types.ObjectId(o));
            await Order.update({ _id: { $in: orderIds } }, {
                $set: {
                    status: "Paid",
                    commission_status: COMMISSION_STATUS.Approved,
                    paypal
                }
            }, { multi: true });
            // sendEmailAndNotificationToMerchant("MER_ORDER", res.locals.user, ORDER_STATUS.Paid);
            // const pendingStock = await getPendingStock(orderIds);
            // await substractPendingStock(pendingStock);
            await RewardPts.update({ order_id: { $in: orderIds } }, { $set: { status: "Awarded" } }, { multi: true });
            // return res.status(200).json({ "message": "Status update to paid and reward points credited." });
            res.locals.orderIds = orderIds;
            next();
        } catch (error) {
            return next(error);
        }
    }
    async function getPendingStock(req: Request, res: Response, next: NextFunction) {
        console.log("Calculating pending stock");
        const { orderIds } = res.locals;
        const pendingStock = await Order.aggregate(
            {
                $match: {
                    "status": { $eq: "Paid" },
                    "_id": { $in: orderIds }
                }
            },
            {
                $unwind: "$products"
            },
            {
                $unwind: "$products.variants"
            },
            {
                $group: {
                    "_id": "$products.product._id",
                    result: {
                        $push: {
                            product_id: "$products.product._id",
                            variant_id: "$products.variants._id",
                            "total": { $sum: "$products.variants.order_qty" }
                        }
                    }
                }
            },
            {
                "$unwind": "$result"
            },
            {
                $project: {
                    product_id: "$result.product_id",
                    variant_id: "$result.variant_id",
                    "total": "$result.total"
                }
            }
        ).exec();
        res.locals.pendingStock = pendingStock;
        next();
    }
    async function substractPendingStock(req: Request, res: Response, next: NextFunction) {
        const { pendingStock } = res.locals;
        const variant = [];
        const product = [];
        const r = await Promise.all(pendingStock.map(p => {
            const {
                product_id: productId,
                variant_id: variantId,
                total } = p;
            if (!variantId) {
                product.push(Types.ObjectId(productId));
                return Product.findByIdAndUpdate(productId, { $inc: { "brief.stock": -total, "stock.qty": -total } });
            } else {
                variant.push(Types.ObjectId(variantId));
                return Variant.findByIdAndUpdate(variantId, { $inc: { "stock": -total } });
            }
        }));
        await Product.update({ _id: { $in: product }, "brief.stock": { "$lte": 0 } }, { $set: { is_active: false } });
        await Variant.update({ _id: { $in: variant }, "stock": { "$lte": 0 } }, { $set: { is_active: false } });
        next();
    }
    async function buyerConfirmOrder(req: Request, res: Response, next: NextFunction) {
        try {
            const { order_id } = req.query;
            const buyerId = req["user_id"];
            const order = await Order.findOne(
                {
                    "_id": Types.ObjectId(order_id),
                    "buyer_id": Types.ObjectId(buyerId)
                });
            if (!order) { return next("No such order or illegal access"); }
            if (ORDER_STATUS.Delivering !== order.status) return next("Invalid order status");
            order.status = ORDER_STATUS.GoodReceived;
            order["commission_status"] = COMMISSION_STATUS.Released;
            await order.save();
            return res.status(200).send("Item delivered and received.");
        } catch (error) {
            return next(error);
        }
    }
    async function merchantProccedToNextStageOfOrder(req: Request, res: Response, next: NextFunction) {
        try {
            const { shipment_id, order_id } = req.body;
            const user_id = req["user_id"];
            const order = await Order.findById(order_id);
            switch (order["status"]) {
                case ORDER_STATUS.GoodReceived:
                    return res.status(200).send("Goods have already benn received.");
                case ORDER_STATUS.AwaitingDelivery:
                    if (!shipment_id) return next("Shipment id is required");
                    await Order.findByIdAndUpdate(order_id, {
                        $set: {
                            "delivery.shipment_id": shipment_id,
                            "status": ORDER_STATUS.Delivering
                        }
                    });
                    return res.status(200).send("Sending request to detemine if good is delivered");
                case ORDER_STATUS.Delivering:
                    return res.status(200).send("Goods are delivering");
                case ORDER_STATUS.Paid:
                    await Order.findByIdAndUpdate(order_id, shipment_id ? {
                        $set: {
                            "delivery.shipment_id": shipment_id, "status": ORDER_STATUS.Delivering
                        }
                    } : { $set: { "status": ORDER_STATUS.AwaitingDelivery } });
                    return res.status(200).send("Your item status has been changed");
                default:
                    return next("Invalid status for operation");
            }

        } catch (error) {
            return next(error);
        }
    }
    async function buyerPostComplaint(req: Request, res: Response, next: NextFunction) {
        try {
            const {
                order_id,
                comment
            } = req.body;
            const buyer_id = req["user_id"];
            if (await Complaint.findOne({
                order_id: Types.ObjectId(order_id),
                buyer_id: Types.ObjectId(buyer_id)
            })) {
                return res.status(409).send({ message: "Complaint existed" });
            }

            const newComplaint = new Complaint({
                order_id,
                comment,
                buyer_id
            });
            await newComplaint.save();
            return res.status(200).send({ "message": "Complaint created." });
        } catch (err) {
            return next(err);
        }
    }
    async function report(req: Request, res: Response, next: NextFunction) {
        const commission = await
            Order.aggregate(
                // Limit to relevant documents and potentially take advantage of an index
                [{
                    $match: {
                        "commission_status": "Approved",
                        "merchant_id": req["user_id"]
                    }
                },
                {
                    $group: {
                        _id: null,
                        total: { $sum: "$total.commission" }
                    }
                }]
            );
        const order = await Order.aggregate(
            [{
                $match: {
                    "merchant_id": req["user_id"]
                }
            },
            {
                $group: {
                    _id: null,
                    qty: { $sum: 1 },
                    amt: { $sum: "$total.store_ap" }
                }
            }]);
        const paid = await Order.aggregate(
            [{
                $match: {
                    "status": "Paid",
                    "merchant_id": req["user_id"]
                }
            },
            {
                $group: {
                    _id: null,
                    qty: { $sum: 1 },
                    amt: { $sum: "$total.store_ap" }
                }
            }]);
        const result = {
            total: {
                order: {
                    all: order,
                    paid,
                    AD: await Order.count({ "merchant_id": req["user_id"], "status": "AD" }),
                    DG: await Order.count({ "merchant_id": req["user_id"], "status": "DG" }),
                    GR: await Order.count({ "merchant_id": req["user_id"], "status": "GR" }),
                },
                products: {
                    qty: await Product.count({ "merchant_id": req["user_id"] }),
                    "pending": await Product.count({ "merchant_id": req["user_id"], "status": "Pending" }),
                    "approved": await Product.count({ "merchant_id": req["user_id"], "status": "Approved" }),
                    "rejected": await Product.count({ "merchant_id": req["user_id"], "status": "Rejected" }),
                },
                commission: {
                    approved: commission
                },
            }
        };
        return res.status(200).send(result);
    }
    async function pendingStock(req: Request, res: Response, next: NextFunction) {
        const products = await Product.update({ "brief.price": { $exists: false } }, { $set: { status: PRODUCT_STATUS.Rejected } }, { multi: true });

        return res.status(200).send("ok");
    }
    async function deleteOrder(req: Request, res: Response, next: NextFunction) {
        try {
            const { order } = req.body;
            const order_id = [].concat(order).map(_id => Types.ObjectId(_id));
            const orderRemoved = await Order.remove({ "_id": { $in: order_id } });
            debug(`orderRemoved ${order_id}`);
            await RewardPts.remove({ "order_id": { $in: order_id } });
            await Complaint.remove({ "order_id": { $in: order_id } });

            return res.status(200).send({
                "message": "Order removed"
            });
        } catch (e) {
            debug(`error ${e}`);
            return next(e);
        }
    }
    async function adminSearchOrder(req: Request, res: Response, next: NextFunction) {
        const orders = await Order.find(req.body.query).populate("buyer_id merchant_id", "contact profile", User);
        return res.status(200).send(orders);
    }
    async function adminUpdateOrderStatus(req: Request, res: Response, next: NextFunction) {
        try {

            const { order_id } = req.query;
            const { status } = req.body;
            if (["AD", "DG", "GR"].indexOf(status) < 0) {
                return next(`Unkown status ${status}`);
            }
            const result = await Order.findByIdAndUpdate(order_id, { $set: { status } });
            if (!result) { res.status(200).send("No result found"); }
            return res.status(200).send(`Order status updated to ${status}`);
        } catch (e) {
            res.status(500);
            return next(e);
        }
    }
    async function adminGetOrderComplain(req: Request, res: Response, next: NextFunction) {
        try {
            return res.status(200).send(await Complaint.find());
        }
        catch (err) {
            return next(err);
        }
    }

    async function updateOrderStatus(req: Request, res: Response, next: NextFunction) {
        console.log("updateOrderStatus");
        try {
            const {
                order_id,
                status
            } = req.body;
            console.log("updateOrderStatus", req.body);
            const orderIds = [].concat(order_id).map(o => Types.ObjectId(o));
            await Order.update({ _id: { $in: orderIds } }, {
                $set: {
                    status: status

                }
            }, { multi: true });
            // sendEmailAndNotificationToMerchant("MER_ORDER", res.locals.user, ORDER_STATUS.Paid);
            // const pendingStock = await getPendingStock(orderIds);
            // await substractPendingStock(pendingStock);
            // await RewardPts.update({ order_id: { $in: orderIds } }, { $set: { status: "Awarded" } }, { multi: true });
            return res.status(200).json({ "message": "Status Changed" });
            // res.locals.orderIds = orderIds;
            // next();
        } catch (error) {
            return res.status(500).send(error);
        }
    }
    return {
        getDeliveryDetail,
        getOrderDetail,
        getPromotionDetailFromPromoCode,
        processStore,
        buyerConfirmOrder,
        adminGetOrder: getOrder("admin"),
        placeOrder,
        pendingStock,
        buyerMadePayment,
        getPendingStock,
        substractPendingStock,
        buyerPostComplaint,
        buyerGetOrder: getOrder("buyer"),
        merchantGetOrder: getOrder("merchant"),
        merchantProccedToNextStageOfOrder,
        buyerPostComment: commentCtrl(config.role).postComments,
        report,
        adminSearchOrder,
        adminUpdateOrderStatus,
        adminGetOrderComplain,
        deleteOrder,
        updateOrderStatus
    };
};

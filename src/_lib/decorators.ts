export function More(info?) {
    return (target, propertyKey: string, descriptor: PropertyDescriptor) => {
        const oldValue = descriptor.value;
        descriptor.value = async function () {
            const value = await oldValue.apply(this, arguments);
            return arguments[1].status(value.status_code).json({
                ...value,
                ... (value.detail_code ? info[value.detail_code] : {})
            });
        };
        return descriptor;
    };
}
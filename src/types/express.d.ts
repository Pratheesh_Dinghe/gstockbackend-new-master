import { UserModel } from "../models/User";
import { AdminModel } from "../models/Admin";
declare module 'express' {
    export interface Response {
        locals: {
            user?: UserModel
            admin?: AdminModel
            welcome?: boolean
            pendingStock: any;
            orderIds: any;
            promotions: any;
            orderDetails: any;
            cart: any;
            orderForReport: any;
        }
    }
}


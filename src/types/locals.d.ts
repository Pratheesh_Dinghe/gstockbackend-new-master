import { UserModel } from "../models/User";
import { AdminModel } from "../models/Admin";
// import * as Express from "Express";
declare global {
    namespace Express {
        export interface Request {
            locals: {
                user?: UserModel
                admin?: AdminModel
            }
        }
        export interface Response {
            locals: {
                user?: UserModel;
                admin?: AdminModel;

            }
        }
    }
}


import * as mongoosePaginate from "mongoose-paginate";
import { Model, Document, Connection, Types, PaginateModel } from "mongoose";
import productAttributeSchema, { ProductAttributeModel } from "./ProductAttribute";
import rewardPtsSchema, { RewardPtsModel } from "./RewardPts";
import variantSchema, { VariantModel } from "./Variants";
import adminSchema, { AdminModel } from "./Admin";
import userSchema, { UserModel } from "./User";
import buyerSchema, { BuyerModel } from "./Buyer";
import merchantSchema, { MerchantModel } from "./Merchant";
import productSchema, { ProductModel } from "./Product";
import categorySchema, { CategoryModel } from "./Category";
import cartSchema, { CartModel } from "./Cart";
import orderSchema, { OrderModel } from "./Order";
import commentSchema, { CommentModel } from "./Comment";
import promotionSchema, { PromotionModel } from "./Promotion";
import attributeSchema, { AttributeModel } from "./Attribute";
import pendingStockSchema, { PendingStockModel } from "./PendingStock";
import creditWalletSchema, { CreditWalletModel } from "./CreditWallet";
import complaintSchema, { ComplaintModel } from "./Complaint";
import rewardProductSchema, { RewardProductModel } from "./RewardProdcut";
import emailSchema, { EmailModel } from "./Email";


export interface AdminModels {
    User: PaginateModel<UserModel>;
    Buyer: PaginateModel<BuyerModel>;
    Admin: Model<AdminModel>;
    Product: PaginateModel<ProductModel>;
    Category: Model<CategoryModel>;
    Order: Model<OrderModel>;
    Cart: Model<CartModel>;
    Promotion: Model<PromotionModel>;
    Attribute: Model<AttributeModel>;
    Variant: Model<VariantModel>;
    PendingStock: Model<PendingStockModel>;
    RewardPts: Model<RewardPtsModel>;
    ProductAttribute: Model<ProductAttributeModel>;
    Comment: Model<CommentModel>;
    CreditWallet: Model<CreditWalletModel>;
    Complaint: Model<ComplaintModel>;
    RewardProdcut: Model<RewardProductModel>;
    Email: Model<EmailModel>;
}
export interface BuyerModels {
    User: PaginateModel<UserModel>;
    Buyer: PaginateModel<BuyerModel>;
    Product: PaginateModel<ProductModel>;
    Category: Model<CategoryModel>;
    Order: Model<OrderModel>;
    Cart: Model<CartModel>;
    Promotion: Model<PromotionModel>;
    Attribute: Model<AttributeModel>;
    Variant: Model<VariantModel>;
    RewardPts: Model<RewardPtsModel>;
    ProductAttribute: Model<ProductAttributeModel>;
    Comment: Model<CommentModel>;
    CreditWallet: Model<CreditWalletModel>;
    Complaint: Model<ComplaintModel>;
    RewardProdcut: Model<RewardProductModel>;
    Email: Model<EmailModel>;
}
export interface MerchantModels {
    User: PaginateModel<UserModel>;
    Product: PaginateModel<ProductModel>;
    Category: Model<CategoryModel>;
    Order: Model<OrderModel>;
    Attribute: Model<AttributeModel>;
    Variant: Model<VariantModel>;
    PendingStock: Model<PendingStockModel>;
    ProductAttribute: Model<ProductAttributeModel>;
    Comment: Model<CommentModel>;
    Complaint: Model<ComplaintModel>;
    Email: Model<EmailModel>;
}
export class Models2 {
    constructor(private role: "admin" | "buyer" | "merchant", private connection: Connection) {
    }

    admin(): AdminModels {
        return {
            User: this.connection.model("User", userSchema) as PaginateModel<UserModel>,
            Buyer: this.connection.model("Buyer", buyerSchema) as PaginateModel<BuyerModel>,
            Admin: this.connection.model("Admin", adminSchema),
            Cart: this.connection.model("Cart", cartSchema),
            RewardPts: this.connection.model("RewardPts", rewardPtsSchema),
            CreditWallet: this.connection.model("Credit", creditWalletSchema),
            Product: this.connection.model("Product", productSchema) as PaginateModel<ProductModel>,
            Category: this.connection.model("Category", categorySchema),
            Promotion: this.connection.model("Promotion", promotionSchema),
            Attribute: this.connection.model("Attribute", attributeSchema),
            Variant: this.connection.model("Variant", variantSchema),
            PendingStock: this.connection.model("PendingStock", pendingStockSchema),
            ProductAttribute: this.connection.model("ProductAttribute", productAttributeSchema),
            Comment: this.connection.model("Comment", commentSchema),
            RewardProdcut: this.connection.model("RewardProduct", rewardProductSchema),
            Email: this.connection.model<EmailModel>("Email", emailSchema),
            Order: this.connection.model("Order", orderSchema),
            Complaint: this.connection.model("Complaint", complaintSchema)
        };
    }
    buyer(): BuyerModels {
        return {
            User: this.connection.model("User", userSchema) as PaginateModel<UserModel>,
            Buyer: this.connection.model("Buyer", buyerSchema) as PaginateModel<BuyerModel>,
            Cart: this.connection.model("Cart", cartSchema),
            RewardPts: this.connection.model("RewardPts", rewardPtsSchema),
            CreditWallet: this.connection.model("Credit", creditWalletSchema),
            Product: this.connection.model("Product", productSchema) as PaginateModel<ProductModel>,
            Category: this.connection.model("Category", categorySchema),
            Promotion: this.connection.model("Promotion", promotionSchema),
            Attribute: this.connection.model("Attribute", attributeSchema),
            ProductAttribute: this.connection.model("ProductAttribute", productAttributeSchema),
            Variant: this.connection.model("Variant", variantSchema),
            Comment: this.connection.model("Comment", commentSchema),
            RewardProdcut: this.connection.model("RewardProduct", rewardProductSchema),
            Email: this.connection.model<EmailModel>("Email", emailSchema),
            Order: this.connection.model("Order", orderSchema),
            Complaint: this.connection.model("Complaint", complaintSchema)
        };
    }
    merchant(): MerchantModels {
        return {
            User: this.connection.model("User", userSchema) as PaginateModel<UserModel>,
            Product: this.connection.model("Product", productSchema) as PaginateModel<ProductModel>,
            Category: this.connection.model("Category", categorySchema),
            Attribute: this.connection.model("Attribute", attributeSchema),
            Variant: this.connection.model("Variant", variantSchema),
            Comment: this.connection.model("Comment", commentSchema),
            Email: this.connection.model<EmailModel>("Email", emailSchema),
            ProductAttribute: this.connection.model("ProductAttribute", productAttributeSchema),
            Order: this.connection.model("Order", orderSchema),
            PendingStock: this.connection.model("PendingStock", pendingStockSchema),
            Complaint: this.connection.model("Complaint", complaintSchema)
        };
    }
    models(): AdminModels | BuyerModels | MerchantModels {
        return {
            admin: this.admin(),
            merchant: this.merchant(),
            buyer: this.buyer()
        }[this.role];
    }
}
interface DBS {
    userDB: Connection;
    productDB: Connection;
    orderDB: Connection;
    role: string;
}
const count = 0;
export interface Models {
    User: PaginateModel<UserModel>;
    Buyer: PaginateModel<BuyerModel>;
    Admin: Model<AdminModel>;
    Product: PaginateModel<ProductModel>;
    Category: Model<CategoryModel>;
    Order: Model<OrderModel>;
    Cart: Model<CartModel>;
    Promotion: Model<PromotionModel>;
    Attribute: Model<AttributeModel>;
    Variant: Model<VariantModel>;
    PendingStock: Model<PendingStockModel>;
    RewardPts: Model<RewardPtsModel>;
    ProductAttribute: Model<ProductAttributeModel>;
    Comment: Model<CommentModel>;
    CreditWallet: Model<CreditWalletModel>;
    Complaint: Model<ComplaintModel>;
    RewardProdcut: Model<RewardProductModel>;
    Email: Model<EmailModel>;
}
export default (dbs: DBS): Models => {
    const {
        userDB,
        productDB,
        orderDB,
        role
    } = dbs;

    return {
        User: userDB.model("User", userSchema) as PaginateModel<UserModel>,
        Buyer: userDB.model("Buyer", buyerSchema) as PaginateModel<BuyerModel>,
        Admin: userDB.model("Admin", adminSchema),
        Cart: userDB.model("Cart", cartSchema),
        RewardPts: userDB.model("RewardPts", rewardPtsSchema),
        CreditWallet: userDB.model("Credit", creditWalletSchema),
        Product: productDB.model("Product", productSchema) as PaginateModel<ProductModel>,
        Category: productDB.model("Category", categorySchema),
        Promotion: productDB.model("Promotion", promotionSchema),
        Attribute: productDB.model("Attribute", attributeSchema),
        Variant: productDB.model("Variant", variantSchema),
        PendingStock: productDB.model("PendingStock", pendingStockSchema),
        ProductAttribute: productDB.model("ProductAttribute", productAttributeSchema),
        Comment: productDB.model("Comment", commentSchema),
        RewardProdcut: productDB.model("RewardProduct", rewardProductSchema),
        Email: orderDB.model<EmailModel>("Email", emailSchema),
        Order: orderDB.model("Order", orderSchema),
        Complaint: orderDB.model("Complaint", complaintSchema),
    };
};
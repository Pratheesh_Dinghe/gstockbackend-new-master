import * as bcrypt from "bcrypt-nodejs";
import * as crypto from "crypto";
import { Document, Error, Schema, SchemaTypes, Types } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";
import { ObjectID } from "bson";
export interface MerchantCommission {
  category_id: Types.ObjectId;
  rate: number;
}
export interface UserModel extends Document {
  is_active: boolean;
  detail: {
    shop_title: string
  };
  credential: {
    fb_id: string;
    user_name: string;
    email: string;
    profile_pic: string;
    password: string;
    last_login: Date;
    password_reset_token: string;
    password_reset_expires: Date;
    user_group: string;
    email_verified: boolean;
    email_verif_token: string;
    email_verif_expires: Date;
  };
  profile: {
    profile_url: string,
    associate_to_website: string,
    DOB: Date,
    gender: string,
    prefix: string,
    first_name: string,
    middle_name: string,
    last_name: string,
    suffix: string,
  };
  contact: {
    email_for_contact: string,
    mobile_no: string,
    tel: number,
    fax: number,
    skype: string,
  };
  company: {
    company_name: string,
    person_charge: string,
    reg_no: string,
  };
  bank: {
    title: string,
    name: string,
    code: string,
    holder_name: string,
    account_no: string,
    merchant_status: string
  };
  location: {
    addressline1: string,
    addressline2: string,
    addressline3: string,
    country: string,
    state_province: string,
    postal_code: string
  };
  payment: {
    tax_number: string,
    vat_number: number,
    payment_info: string,
    default_billing_add: string
  };
  commission: [MerchantCommission];
  store: {
    name: string,
    description: string
  };
  comparePassword: (candidatePassword: string, password: string, cb: (err: any, isMatch: any) => void) => void;
  gravatar: (size: number) => string;
  hide: string;
}
const userSchema = new Schema({
  is_active: {
    type: Boolean,
    default: true
  },
  detail: {
    shop_title: String
  },
  credential: {
    select: false,
    type: {
      fb_id: String,
      user_name: String,
      email: { type: String, unique: true },
      password: {
        type: String,
        select: false
      },
      user_group: String,
      last_login: Date,
      password_reset_token: String,
      password_reset_expires: Date,
      email_verified: Boolean,
      email_verif_token: String,
      email_verif_expires: Date,
    },
  },

  profile: {
    type: {
      profile_pic_url: String,
      associate_to_website: String,
      DOB: Date,
      gender: {
        type: String,
        default: ["M"],
        enum: ["M", "F"]
      },
      prefix: String,
      first_name: String,
      middle_name: String,
      last_name: String,
      suffix: String,
    },
  },
  contact: {
    skype: String,
    email_for_contact: String,
    mobile_no: String,
    tel: Number,
    fax: Number
  },
  company: {
    company_name: String,
    person_charge: String,
    reg_no: String,
  },
  bank: {
    code: String,
    title: String,
    name: String,
    holder_name: String,
    account_no: String,
  },
  location: {
    addressline1: String,
    addressline2: String,
    addressline3: String,
    country: String,
    state_province: String,
    postal_code: String
  },
  payment: {
    category_commission: String,
    tax_number: String,
    vat_number: Number,
    payment_info: String,
    default_billing_add: String
  },
  store: {
    name: String,
    description: String
  },
  commission: [{
    category_id: SchemaTypes.ObjectId,
    rate: Number
  }]
}, {
    timestamps: true,
    minimize: false
  });

/**
 * Password hash middleware.
 */

userSchema.pre("save", function save(next) {
  const user = this;
  if (!user.isModified("credential.password")) {
    return next();
  }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) { return next(err); }
    bcrypt.hash(user.credential.password, salt, undefined, (err: Error, hash) => {
      if (err) { return next(err); }
      user.credential.password = hash;
      next();
    });
  });
});

/**
 * Helper method for getting user's gravatar.
 */

userSchema.methods.gravatar = function (size: number) {
  if (!size) {
    size = 200;
  }
  if (!this.email) {
    return `https://gravatar.com/avatar/?s=${size}&d=retro`;
  }
  const md5 = crypto.createHash("md5").update(this.email).digest("hex");
  return `https://gravatar.com/avatar/${md5}?s=${size}&d=retro`;
};

/**
 * Compare password
 */
userSchema.methods.comparePassword = function (candidatePassword: string, password: string, cb: (err: Error, isMatch: boolean) => {}) {
  bcrypt.compare(candidatePassword, password, function (err: Error, isMatch: boolean) {
    // console.log("Everythin ok?", candidatePassword, password);
    cb(err, isMatch);
  });
};

userSchema.virtual("hide")
  .get(function () { return this; })
  .set(function (field) {
    this[field] = {};
    // console.log("Calling virtual and deleting credential", this);
  });
userSchema.plugin(mongoosePaginate);
export default userSchema;
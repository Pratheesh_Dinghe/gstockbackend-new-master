import { NextFunction, Request, Response, Router } from "express";
import { checkSchema } from "express-validator/check";
import { validationResponse } from "../../_middleware/validationRes";
import { orderController, rewardProductController } from "./../../controllers";

export default config => {
  console.log("HELLOOOOOOOO");
  const {
    getDeliveryDetail,
    getOrderDetail,
    getPromotionDetailFromPromoCode,
    processStore,
    placeOrder,
    buyerMadePayment,
    getPendingStock,
    substractPendingStock,
    pendingStock
  } = orderController(config);
  const { purchaseRewardProduct } = rewardProductController(config);
  return (
    Router()
      // .get("/", async (req: Request, res: Response, next: NextFunction) => {
      //     const orders = await models.Order.find({});

      //     // { $and: [{ "createdAt": { $gte: req.body.start } }, { "createdAt": { $gte: req.body.end } }]

      //     // if (err) return next(err);
      //     return res.status(200).send(orders);
      // })
      // .post("/place", checkSchema({
      //     "orders.*.products.*.variants.*.qty": {
      //         in: ["body"],
      //         isInt: {
      //             options: { min: 1 },
      //             errorMessage: "Product quantity must not be lower than 1.",
      //         }
      //     }
      // })
      // .post("/place", [validationResponse, getDeliveryDetail, getOrderDetail, getPromotionDetailFromPromoCode, processStore, placeOrder])
      .post("/place", [
        getDeliveryDetail,
        getOrderDetail,
        getPromotionDetailFromPromoCode,
        processStore,
        placeOrder
      ])
      .post(
        "/success",
        [buyerMadePayment, getPendingStock, substractPendingStock],
        async (req: Request, res: Response, next: NextFunction) => {
          return res.status(200).send(res.locals);
        }
      )
      .get("/pending", pendingStock)
      .post("/reward-product/order", purchaseRewardProduct)
  );
};

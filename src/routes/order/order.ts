import { NextFunction, Request, Response, Router } from "express";
import { checkSchema } from "express-validator/check";
import { validationResponse } from "../../_middleware/validationRes";
import { orderController, rewardProductController } from "../../controllers";
import * as nodemailer from "nodemailer";
import * as fs from "fs";
import * as Handlebars from "handlebars";
import * as dateFormat from "dateformat";

export default config => {
  const {
    getDeliveryDetail,
    getOrderDetail,
    // getSalesReturnOrders,
    getPromotionDetailFromPromoCode,
    processStore,
    placeOrder,
    buyerMadePayment,
    getPendingStock,
    substractPendingStock,
    pendingStock,
    updateOrderStatus
    //    AddshipTo
  } = orderController(config);
  const { Order, User } = config.models;
  const populate = {
    path: "buyer_id merchant_id",
    select: "contact profile credential.email",
    model: User
  };
  console.log("PRATHEESH_02");
  const { purchaseRewardProduct } = rewardProductController(config);
  console.log("PRATHEESH_04");
  return (
    Router()
      // .get("/", async (req: Request, res: Response, next: NextFunction) => {
      //     const orders = await models.Order.find({});

      //     // { $and: [{ "createdAt": { $gte: req.body.start } }, { "createdAt": { $gte: req.body.end } }]

      //     // if (err) return next(err);
      //     return res.status(200).send(orders);
      // })
      // Commented By Pratheesh : 281118
      //  .post("/place", checkSchema({
      //     "orders.*.products.*.variants.*.qty": {
      //         in: ["body"],
      //         isInt: {
      //             options: { min: 1 },
      //             errorMessage: "Product quantity must not be lower than 1.",
      //         }
      //     }
      // }), [validationResponse, getDeliveryDetail, getOrderDetail, getPromotionDetailFromPromoCode, processStore, placeOrder])
      .post("/place", [
        validationResponse,
        getDeliveryDetail,
        getOrderDetail,
        getPromotionDetailFromPromoCode,
        processStore,
        placeOrder
      ])

      // .post("/place", checkSchema({
      //     "orders.*.products.*.variants.*.qty": {
      //         in: ["body"],
      //         isInt: {
      //             options: { min: 1 },
      //             errorMessage: "Product quantity must not be lower than 1.",
      //         }
      //     }
      // }), [validationResponse, getDeliveryDetail, getOrderDetail, getPromotionDetailFromPromoCode, processStore, placeOrder])
      // .post("/success", [buyerMadePayment, getPendingStock, substractPendingStock, getOrderDetail], async (req: Request, res: Response, next: NextFunction) => {
      //     return res.status(200).send(res.locals);
      .post(
        "/success",
        [buyerMadePayment, getPendingStock, substractPendingStock],
        async (req: Request, res: Response, next: NextFunction) => {
          // console.log("REQ", req.body);
          // const { order_id } = req.body;
          // console.log("ORDER", order_id);
          // const orders = await Order.findById(order_id).populate(populate);
          // const root = "./src/emailTemplate";
          // console.log(root);
          // const imagesPath = `${root}/welcome/images`;
          // const templatePath = `${root}/order.html`;
          // let mailOptions = {};
          // const smtpTransport = nodemailer.createTransport({
          //   port: 587,
          //   host: "smtp.office365.com",
          //   secure: false,
          //   auth: {
          //     user: "glux@dinghe.sg",
          //     pass: "Lamsoon8226"
          //   },
          //   tls: {
          //     rejectUnauthorized: false
          //   }
          // });
          // fs.readFile(templatePath, (err, chunk) => {
          //   if (err) {
          //     return next(err);
          //   }
          //   const html = chunk.toString("utf8");
          //   const template = Handlebars.compile(html);
          //   const current_time = new Date();
          //   const someDate = new Date();
          //   const numberOfDaysToAdd = 3;
          //   someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
          //   const dd = someDate.getDate();
          //   const mm = someDate.getMonth() + 1;
          //   const y = someDate.getFullYear();
          //   const deliveryDate = dd + "/" + mm + "/" + y;
          //   const imgSrc =
          //     "https://buyerservices.glux.sg/uploads/user/undefined/products/";
          //   const data = {
          //     fromEmail: "glux@dinghe.sg",
          //     mail_obj: orders,
          //     productArray: orders.products,
          //     time: dateFormat(current_time, "shortTime"),
          //     order_date: dateFormat(orders.createdAt, "mediumDate"),
          //     imgSrc,
          //     deliveryDate
          //   };
          // const result = template(data);
          // console.log(orders.buyer_id.credential.email, "email");
          // mailOptions = {
          // LIVE
          // to: [orders.buyer_id.credential.email, "glux@dinghe.sg"] ,
          // bcc: ["jhothi@dinghe.sg", "angi@dinghe.sg"],
          //       to: [orders.buyer_id.credential.email, "glux@dinghe.sg"] ,
          //       bcc: ["pratheesh@dinghe.sg", "dhania@dinghe.sg"],
          //       from: "glux@dinghe.sg",
          //       subject: "Thank you for shopping on GLUX by gstock.",
          //       html: result
          //     };
          //       const sendEmail = new Promise((resolve, reject) => {
          //       smtpTransport.sendMail(mailOptions, (error, response: any) => {
          //         if (error) {
          //           console.log(error);
          //           reject(error);
          //         } else {
          //           console.log(response);
          //           resolve(response);
          //         }
          //       });
          //     });
          //   });
          //   // -------------------------------
          //   return res.status(200).send(orders);
        }
      )
      .get("/pending", pendingStock)
      // .get("/getOrder-SalesReturn", getSalesReturnOrders)
      //   .get("/SalesReturnOrders", getSalesReturnOrders)
      .post("/reward-product/order", purchaseRewardProduct)
  );
};

import { Request, Response, NextFunction, Router } from "express";
import * as PDFkit from "pdfkit";
import orderController from "./../../../../controllers/order";
/**
 * Order Management
 */
export default (config) => {
    const {
        deleteOrder,
        adminGetOrder,
        adminSearchOrder,
        adminGetOrderComplain
    } = orderController(config);
    return Router()
        .get("/", adminGetOrder)
        .post("/search", adminSearchOrder)
        .put("/status", )
        // .get("/invoice", async (req: Request, res: Response, next: NextFunction) => {
        //     console.log("invoice");
        //     const doc = new PDFkit;
        //     doc.on("pageAdded", () => {
        //         doc.circle(280, 200, 50).fill("#6600FF");
        //     });
        //     try {
        //         doc.pipe(res);
        //         doc.text("My man!!!!");
        //         doc.addPage();
        //     } catch (e) {
        //         return res.status(400).send(e);
        //     }
        //     doc.end();
        //     return;
        // })
        .get("/complaint", adminGetOrderComplain)
        .post("/bulk/delete", deleteOrder);
};
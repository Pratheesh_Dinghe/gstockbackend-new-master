import { Request, Response, NextFunction, Router } from "express";
import promotionController from "../../../../controllers/promotion";
import { checkSchema } from "express-validator/check";
import { validationResponse } from "../../../../_middleware/validationRes";
/*
 * Promotion management
 */
export default (config) => {
    const {
        createNewPromotion,
        getPromotion,
        updatePromotion,
        updateStatus
    } = promotionController(config);
    return Router()
        .post("/", createNewPromotion)
        .get("/", getPromotion)
        .put("/", updatePromotion)
        .put("/status", updateStatus);
};
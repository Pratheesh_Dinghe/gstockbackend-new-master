import orderRoutes from "./order/order";
import { ProductRoutes } from "./product/product";
import mgmtRoute from "./user/admin/management";
import { BuyerRoutes } from "./user/buyer/buyer";
import merchantRoutes from "./user/merchant/merchant";
import { UserRoutes } from "./user/user";
import logisticRoute from "./order/logistics";
import adminRoute from "./../controllers/user/admin/admin";

export default {
    orderRoutes,
    ProductRoutes,
    ...mgmtRoute,
    BuyerRoutes,
    merchantRoutes,
    adminRoute,
    logisticRoute,
    UserRoutes,
};